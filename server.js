const express =require('express');
const app= express();
const port= process.env.PORT || 3000;
const bodyParser =require('body-parser');
app.use(bodyParser.json());//utiliza el parse de json
app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"hola desde API TechU"});
  }
);


app.get("/apitechu/v1/users",
 function(req, res) {
   console.log("GET /apitechu/v1/users");
   console.log(req.query);

   var result = {};
   var users = require('./clientes.json');
   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length;
   }

   result.users = req.query.$top ?
      users.slice(0, req.query.$top) : users;
      res.send(result);

 }
);


app.post('/apitechu/v1/login',
         function(req,res){
           console.log("POST /apitechu/v1/login" );
           var clients = require('./clientes.json');
           var result={};

           for (client of clients) {
                    if (client.email == req.body.email) {
                    console.log("encontrado " + req.body.email);
                     if (client.password == req.body.password) {
                          console.log("password ok ");
                          client.logged = true;

                          //res.send({"msg" : "Login Correcto"});
                          result.mensaje= "Login correcto";
                          result.id=client.id;
                          writeUserDataToFile(clients);
                          console.log("cliente logado añadido");

                             break;
                       } else {
                         console.log("password KO ");
                         result.mensaje="login incocrrecto";
                         //res.send({"msg" : "Login Incorrecto"});
                         break;
                         }
                 }
             }
             res.send(result);
         }
     );

     app.post('/apitechu/v1/logout',
              function(req,res){
                console.log("POST /apitechu/v1/logout" );
                var clients = require('./clientes.json');
                var result={};

                for (client of clients) {
                //  console.log(id + req.body.id);
                         if (client.id == req.body.id && client.logged == true){
                         console.log("encontrado " + req.body.id);
                               delete client.logged;
                               writeUserDataToFile(clients);
                               result.mensaje= "logout correcto";
                               result.id=client.id;
                               break;
                            } else {
                              mensaje= "Logout Incorrecto";
                              }
                      }
                        res.send(result);
                  }

          );


app.delete("/apitechu/v1/users/:id",
function(req, res) {
  console.log("DELETE apitechu/v1/users:id");
  console.log("id es "+  req.params.id);
  var users =require('./users.json');
  /*for (user of users) {
    console.log("Length of array is "+  users.length);
    if (user != null && req.params.id == user.id) {
      console.log("La id coincide");
      delete users[user.id -1];
      break;
    }
  } */

  for (arrayId in users){

    if (users[arrayId].id == req.params.id){
      console.log("se ha borrado el usuario de id " + users[arrayId].id);
      users.splice(arrayId,1);
      break;
   }
  }
  writeUserDataToFile(users);
  console.log("Usuario borrado");
  res.send({"msg":"Usuario borrado"});
  }
  );

function writeUserDataToFile(data) {
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./clientes.json",jsonUserData, "utf-8",
    function(err) {
      if (err){
        console.log(err);
      } else {
        console.log("Datos escritos en fichero");
      }
    }
)
}
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body); //necesita un pareseador

  }
);
